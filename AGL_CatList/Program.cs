﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using AGL_CatList.Models;
using System.Configuration;

namespace AGL_CatList
{
    internal class Program
    {

        private static void Main()
        {
            var url = ConfigurationManager.AppSettings[Constants.ApiUrlKey];
            var client = new HttpClient { BaseAddress = new Uri(url) };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonMediaType));

            try
            {
                var response = client.GetAsync(string.Empty).Result;
                if (response.IsSuccessStatusCode)
                {
                    var owners =
                        Newtonsoft.Json.JsonConvert.DeserializeObject<List<Owner>>(response.Content.ReadAsStringAsync()
                            .Result);
                    foreach (var gender in owners.Select(x => x.Gender).Distinct())
                    {
                        Console.WriteLine(gender);
                        foreach (var pet in owners
                            .Where(x => x.Gender == gender && x.Pets != null)
                            .SelectMany(x => x.Pets)
                            .Where(x => x.Type.ToLower() == Constants.Cat))
                        {
                            Console.WriteLine($"\t{pet.Name}");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int) response.StatusCode, response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadKey();
        }
    }
}
