﻿namespace AGL_CatList
{
    public static class Constants
    {
        public const string Cat = "cat";
        public const string JsonMediaType = "application/json";
        public const string ApiUrlKey = "ApiUrlKey";
    }
}
