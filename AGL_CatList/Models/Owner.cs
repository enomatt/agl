﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AGL_CatList.Models
{
    public class Owner
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("age")]
        public int Age { get; set; }

        [JsonProperty("pets")]
        public IEnumerable<Pet> Pets { get; set; }
    }
}
